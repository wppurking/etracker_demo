json.ID listing.id
json.asin listing.asin
json.url listing.url
json.show_url listing_url(listing, format: 'json')
json.created_at listing.created_at
json.updated_at listing.updated_at