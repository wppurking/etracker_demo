class ChannelsController < ApplicationController
  def index
    @channels = Channel.all
  end

  def new
    @channel = Channel.new
  end

  def create
    @channel = Channel.new(new_channel_params)
    if @channel.save
      redirect_to @channel
    else
      render "new"
    end
  end

  def show
    @channel = Channel.find(params[:id])
  end

  def edit
    @channel = Channel.find(params[:id])
  end

  def update
    @channel = Channel.find(params[:id])
    if @channel.update(update_channel_params)
      redirect_to @channel
    else
      render "edit"
    end
  end

  def destroy
    if Channel.delete(params[:id]) <= 0
      flash[:danger] = "删除 Channel #{params[:id]} 失败"
    end
    redirect_to channels_url
  end

  private
  def new_channel_params
    params.require(:channel).permit(:name)
  end

  def update_channel_params
    params.require(:channel).permit(:name)
  end
end
