class SsoLogin
  attr_reader :username, :password

  USER = {
      "wyatt@easya.cc" => "123456"
  }

  def initialize(username, password)
    @username = username
    @password = password
  end

  # 验证是否登陆成功?
  def verify
    user_exist? && password_correct?
  end

  private
  def user_exist?
    USER.key?(@username)
  end

  def password_correct?
    USER[@username] == @password
  end
end