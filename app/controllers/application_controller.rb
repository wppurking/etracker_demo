class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :text_cookie
  before_action :encypt_cookie
  before_action :signed_cookie
  before_action :require_login, except: [:welcome, :login, :do_login]

  def welcome
  end

  def login
  end

  def require_login
    unless login?
      flash[:danger] = "请登陆后再访问."
      cookies[:callback_url] = request.url
      logger.info("Require_login Action: #{params[:action]}")
      logger.info("Require_login Controller: #{params[:controller]}")
      redirect_to login_url
    end
  end

  def do_login
    # 做登陆操作
    sso = SsoLogin.new(params[:email], params[:password])
    if sso.verify
      cookies.signed[:username] = params[:email]
      flash[:info] = "欢迎 #{params[:email]} 回来"

      if cookies[:callback_url].nil?
        redirect_to root_url
      else
        redirect_to cookies.delete(:callback_url)
      end
    else
      flash[:danger] = "登陆失败, 因为用户名或密码错误"
      redirect_to login_url
    end
  end

  def logout
    # 做登出操作
    cookies.delete(:username)
    flash[:info] = "成功登出"
    redirect_to root_url
  end


  private
  def text_cookie
    unless cookies[:text].blank?
      cookies.delete(:text)
      logger.info("=================Delete Text Cookie===============")
    end
    nb = rand(1000)
    logger.info("Cookie Text: #{nb}")
    cookies[:text] = nb
  end

  def encypt_cookie
    unless cookies[:encrypted].blank?
      cookies.delete(:encrypted)
      logger.info("=================Delete Encrypted Cookie===============")
    end
    nb = rand(1000)
    logger.info("Cookie encrypted: #{nb}")
    cookies.encrypted[:encrypted] = nb
  end

  def signed_cookie
    unless cookies[:signed].blank?
      cookies.delete(:signed)
      logger.info("=================Delete Signed Cookie===============")
    end
    nb = rand(1000)
    logger.info("Cookie signed: #{nb}")
    cookies.signed[:signed] = nb
  end
end
