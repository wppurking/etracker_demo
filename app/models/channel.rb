class Channel < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  before_save :add_edit_times

  has_many :listings

  private
  def add_edit_times
    self.edit_times += 1
  end
end
