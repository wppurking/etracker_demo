class Listing < ActiveRecord::Base
  include HTTParty

  headers("Accept-Encoding" => "gzip", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36")
  debug_output $stdout


  belongs_to :channel
  has_many :listing_histories

  has_and_belongs_to_many :tags

  def new_history
    resp = self.class.get(url)
    history = listing_histories.build.parse(resp.body)
    history.save ? history : nil
  end
end
