class ListingHistory < ActiveRecord::Base
  belongs_to :listing

  def parse(html_body)
    doc = Nokogiri::HTML(html_body)
    self.title = doc.css('#productTitle').text

    merchant_info_links = doc.css('#merchant-info a')
    self.seller_name = if merchant_info_links.size > 0
                         merchant_info_links[0].text
                       else
                         'Amazon.com'
                       end
    self.sale_rank_fragment = doc.css('#SalesRank').inner_html
    self.rating_score = doc.css('#acrPopover').attr('title').to_s.split('out of')[0].strip.to_f
    self.prod_desc = doc.css('#productDescription').inner_html
    self
  end
end
