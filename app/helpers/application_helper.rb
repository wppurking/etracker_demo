module ApplicationHelper
  def active_tab(action)
    #controller_name == action ? 'active' : ''
    params[:controller] == action ? 'active' : ''
  end

  def login?
    !cookies[:username].nil?
  end
end
