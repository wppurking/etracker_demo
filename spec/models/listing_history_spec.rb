require 'spec_helper'

describe ListingHistory do
  def b00bfy59ba
    open('spec/models/B00BFY59BA.html').read.encode('utf-8', 'iso8859-1')
  end

  def b0097befya
    open('spec/models/B0097BEFYA.html').read.encode('utf-8', 'iso8859-1')
  end

  describe '#parse' do
    context 'FBA 正常卖家' do
      before(:all) do
        @history = ListingHistory.new.parse(b00bfy59ba)
      end

      it 'title = Photive 3000mAh Portable Backup External Battery Charger for iPhone 5S, 5C, 5, 4S, Galaxy S5, S4, S3, Note 3, Nexus 4, HTC One, One 2 (M8), Nokia Lumia 520, 1020 and most other Smartphones' do
        expect(@history.title).to eq "Photive 3000mAh Portable Backup External Battery Charger for iPhone 5S, 5C, 5, 4S, Galaxy S5, S4, S3, Note 3, Nexus 4, HTC One, One 2 (M8), Nokia Lumia 520, 1020 and most other Smartphones"
      end

      it 'seller_name = Amazing Deals Online' do
        expect(@history.seller_name).to eq 'Amazing Deals Online'
      end

      it 'sale_rank_fragment 包含 #6,723 in Cell Phones' do
        expect(@history.sale_rank_fragment).to include('#6,723 in Cell Phones')
      end

      it 'prod_desc 包含 Nokia: Lumia 920 900 710 800 / N8 ' do
        expect(@history.prod_desc).to include('Nokia: Lumia 920 900 710 800 / N8 ')
      end

      it 'rating_score = 4.2' do
        expect(@history.rating_score).to eq 4.2
        expect(@history.rating_score.class).to eq Float
      end
    end

    context 'Amazon 自营' do
      before(:all) do
        @history = ListingHistory.new.parse(b0097befya)
      end

      it 'seller_name 为 Amazon 自己' do
        expect(@history.seller_name).to eq 'Amazon.com'
      end
    end
  end
end
