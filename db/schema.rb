# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140804093931) do

  create_table "channels", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "edit_times", default: 0
  end

  add_index "channels", ["name"], name: "index_channels_on_name", unique: true, using: :btree

  create_table "listing_histories", force: true do |t|
    t.text     "title"
    t.string   "price_str"
    t.string   "seller_name"
    t.string   "main_img_url"
    t.text     "sale_rank_fragment"
    t.text     "prod_desc"
    t.float    "rating_score",       limit: 24
    t.integer  "listing_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "listing_histories", ["listing_id"], name: "index_listing_histories_on_listing_id", using: :btree

  create_table "listings", force: true do |t|
    t.string   "asin"
    t.text     "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "channel_id"
  end

  add_index "listings", ["channel_id"], name: "index_listings_on_channel_id", using: :btree

  create_table "listings_tags", id: false, force: true do |t|
    t.integer "listing_id"
    t.integer "tag_id"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
