class CreateListingHistories < ActiveRecord::Migration
  def change
    create_table :listing_histories do |t|
      t.text :title
      t.string :price_str
      t.string :seller_name
      t.string :main_img_url
      t.text :sale_rank_fragment
      t.text :prod_desc
      t.float :rating_score

      t.belongs_to :listing, index: true

      t.timestamps
    end
  end
end
