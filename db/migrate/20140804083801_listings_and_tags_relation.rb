class ListingsAndTagsRelation < ActiveRecord::Migration
  def change
    create_table :listings_tags, id: false do |t|
      t.belongs_to :listing
      t.belongs_to :tag
    end
  end
end
