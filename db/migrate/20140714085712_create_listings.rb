class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :asin
      t.text :url

      t.timestamps
    end
  end
end
