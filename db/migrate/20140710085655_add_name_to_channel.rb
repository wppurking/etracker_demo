class AddNameToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :name, :string
    add_index :channels, :name, unique: true
  end
end
