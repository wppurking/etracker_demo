class AddChannelHasManyListings < ActiveRecord::Migration
  def change
    change_table :listings do |t|
      t.belongs_to :channel
      t.index :channel_id
    end
  end
end
