class AddEditTimeToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :edit_times, :integer, :default => 0
  end
end
